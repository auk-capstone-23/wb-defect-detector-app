from django.contrib import admin
from unfold.admin import ModelAdmin

from .models import (
    AnalysisTake,
    SignalAnalysis,
)


@admin.register(SignalAnalysis)
class SignalAnalysisAdmin(ModelAdmin):
    list_display = (
        "title",
        "created_by",
        "created",
        "modified",
    )
    search_fields = ("title", "description")
    list_filter = ("created", "modified")
    readonly_fields = ("created", "modified")


@admin.register(AnalysisTake)
class AnalysisTakeAdmin(ModelAdmin):
    list_display = ("analysis", "created", "modified")
    search_fields = ("comment",)
    list_filter = ("created", "modified")
    readonly_fields = ("created", "modified")
