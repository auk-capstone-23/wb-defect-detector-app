from django import forms

from .models import AnalysisTake, SignalAnalysis


INPUT_TAILWIND_CLASS = "peer h-full w-full rounded-md border border-blue-gray-200 bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-pink-500 focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50"


class SignalAnalysisForm(forms.ModelForm):
    class Meta:
        model = SignalAnalysis
        fields = ["input_signal", "title", "description"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Apply Tailwind CSS classes to each field
        self.fields["input_signal"].widget.attrs.update(
            {
                "class": "mt-1 block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-indigo-50 file:text-indigo-700 hover:file:bg-indigo-100"
            }
        )
        self.fields["title"].widget.attrs.update({"class": INPUT_TAILWIND_CLASS})
        self.fields["description"].widget.attrs.update({"class": INPUT_TAILWIND_CLASS})


class AnalysisTakeForm(forms.ModelForm):
    class Meta:
        model = AnalysisTake
        fields = ["analysis", "window_size", "overlap", "fs", "comment"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Apply Tailwind CSS classes to each field
        self.fields["window_size"].widget.attrs.update({"class": INPUT_TAILWIND_CLASS})
        self.fields["overlap"].widget.attrs.update({"class": INPUT_TAILWIND_CLASS})
        self.fields["fs"].widget.attrs.update({"class": INPUT_TAILWIND_CLASS})
        self.fields["comment"].widget.attrs.update({"class": INPUT_TAILWIND_CLASS})

        # Set default values for the fields if not already provided
        self.fields["window_size"].initial = (
            self.fields["window_size"].initial or 1024
        )  # Replace 1024 with your default value
        self.fields["overlap"].initial = (
            self.fields["overlap"].initial or 128
        )  # Replace 512 with your default value
        self.fields["fs"].initial = (
            self.fields["fs"].initial or 48000
        )  # Replace 44100 with your default value
