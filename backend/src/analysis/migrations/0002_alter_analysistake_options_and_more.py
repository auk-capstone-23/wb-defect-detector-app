# Generated by Django 4.2.7 on 2023-11-27 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='analysistake',
            options={'verbose_name': 'Analysis Take', 'verbose_name_plural': 'Analysis Takes'},
        ),
        migrations.AlterModelOptions(
            name='signalanalysis',
            options={'verbose_name': 'Signal Analysis', 'verbose_name_plural': 'Signal Analyses'},
        ),
        migrations.AlterField(
            model_name='analysistake',
            name='comment',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='signalanalysis',
            name='input_signal',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='signalanalysis',
            name='signalSpectogram',
            field=models.ImageField(null=True, upload_to=''),
        ),
    ]
