from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_fsm import FSMField, transition
from model_utils.models import TimeStampedModel
from processing.tasks import process_analysis


class SignalAnalysis(TimeStampedModel):
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True
    )
    input_signal = models.FileField(blank=True, null=True)
    title = models.CharField(max_length=255, null=False, blank=False)
    description = models.TextField()
    signalSpectogram = models.ImageField(null=True)
    state = FSMField(default="new")

    class Meta:
        verbose_name = _("Signal Analysis")
        verbose_name_plural = _("Signal Analyses")

    @transition(field=state, source="new", target="in_progress")
    def start_processing(self):
        process_analysis(self.pk)

    @transition(field=state, source=["new", "in_progress"], target="processed")
    def finish_processing(self):
        pass


class AnalysisTake(TimeStampedModel):
    analysis = models.ForeignKey(SignalAnalysis, on_delete=models.CASCADE)

    comment = models.TextField(blank=True, null=True)
    analysis_result = models.JSONField(null=True, blank=True)
    window_size = models.PositiveIntegerField(default=1024)
    overlap = models.PositiveIntegerField(default=128)
    fs = models.PositiveIntegerField(default=48000)
    som_hitmap = models.ImageField(null=True)
    state = FSMField(default="new")

    class Meta:
        verbose_name = _("Analysis Take")
        verbose_name_plural = _("Analysis Takes")

    @transition(field=state, source="new", target="in_progress")
    def start_processing(self):
        pass

    @transition(field=state, source="in_progress", target="processed")
    def finish_processing(self):
        pass

    # FK to SignalAnalysis
    # Comment
    # input params
    # JSON result
    # graph images ...
