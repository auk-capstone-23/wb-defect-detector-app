from rest_framework import serializers
from .models import SignalAnalysis


class SignalAnalysisSerializer(serializers.ModelSerializer):
    created_by = serializers.StringRelatedField(default=serializers.CurrentUserDefault(), read_only=True)

    class Meta:
        model = SignalAnalysis
        fields = ['created_by', 'input_signal', 'title', 'description', 'signalSpectogram']

