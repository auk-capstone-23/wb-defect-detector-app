from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (
    AnalysisTakeDetailView,
    SignalAnalysisCreateView,
    SignalAnalysisDetailView,
    SignalAnalysisListView,
    SignalAnalysisModelViewSet,
    SignalAnalysisProcessingView,
    SignalAnalysisTableView,
    SignalTakesHistoryView,
    TakeProcessingView,
    TakeResultView,
    TakeTempView,
    create_analysis_take,
    csv_preview,
    TakeExisingView,
)

router = DefaultRouter()
router.register("analysis", SignalAnalysisModelViewSet, basename="analysis_")

urlpatterns = [
    path("api/v1/", include(router.urls)),
    path("", SignalAnalysisCreateView.as_view(), name="create_signal_analysis"),
    path("list/", SignalAnalysisListView.as_view(), name="create_signal_analysis"),
    path("table/", SignalAnalysisTableView.as_view(), name="create_signal_analysis"),
    path(
        "<int:pk>/", SignalAnalysisDetailView.as_view(), name="signal_analysis_detail"
    ),
    path(
        "take/<int:pk>/", AnalysisTakeDetailView.as_view(), name="analysis_take_detail"
    ),
    path(
        "take_new/<int:analysis_id>/<int:pk>/",
        TakeTempView.as_view(),
        name="analysis_take_new",
    ),
    path(
        "take_existing/<int:analysis_id>/<int:pk>/",
        TakeExisingView.as_view(),
        name="analysis_take_existing",
    ),
    path(
        "take/history/<int:pk>/",
        SignalTakesHistoryView.as_view(),
        name="analysis_take_history",
    ),
    path(
        "<int:analysis_id>/take/create/",
        create_analysis_take,
        name="create_analysis_take",
    ),
    path("csv-preview/<int:id>/", csv_preview, name="csv_preview"),
    path(
        "processing/<int:analysis_id>/",
        SignalAnalysisProcessingView.as_view(),
        name="analysis_processing",
    ),
    path(
        "<int:analysis_id>/take/processing/",
        TakeProcessingView.as_view(),
        name="take_processing",
    ),
    path("take/<int:pk>/result/", TakeResultView.as_view(), name="take_result"),
]
