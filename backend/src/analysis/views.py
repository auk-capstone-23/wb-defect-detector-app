import csv

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from processing.tasks import process_analysis, process_take
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .forms import AnalysisTakeForm, SignalAnalysisForm
from .models import AnalysisTake, SignalAnalysis
from .serializers import SignalAnalysisSerializer


class SignalAnalysisModelViewSet(LoginRequiredMixin, viewsets.ModelViewSet):
    queryset = SignalAnalysis.objects.all()
    serializer_class = SignalAnalysisSerializer
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class SignalAnalysisCreateView(LoginRequiredMixin, CreateView):
    model = SignalAnalysis
    form_class = SignalAnalysisForm
    template_name = "signal_analysis_main.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        response = super().form_valid(form)
        process_analysis(form.instance.pk)
        # form.instance.start_processing()
        # form.instance.save()
        return response

    def get_success_url(self):
        # Redirect to a detail view for the created object
        return reverse("analysis_processing", kwargs={"analysis_id": self.object.pk})


class SignalAnalysisDetailView(LoginRequiredMixin, DetailView):
    model = SignalAnalysis
    template_name = "signal_analysis_detail.html"


class AnalysisTakeCreateView(LoginRequiredMixin, CreateView):
    model = AnalysisTake
    form_class = AnalysisTakeForm
    template_name = "analysis_take_form.html"

    def form_valid(self, form):
        form.instance.analysis = self.kwargs.get("analysis_id")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super().get_context_data(**kwargs)
        context["analysis"] = self.kwargs.get("analysis_id")
        return context

    def get_success_url(self):
        return reverse("analysis_take_detail", kwargs={"pk": self.object.pk})


def create_analysis_take(request, analysis_id):
    if request.method == "POST":
        form = AnalysisTakeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(
                reverse(
                    "analysis_take_new",
                    kwargs={
                        "pk": form.instance.pk,
                        "analysis_id": form.instance.analysis.pk,
                    },
                )  # replace with take_new_section
            )
        return HttpResponse(f"{form.is_valid()} {form.errors}")
    form = AnalysisTakeForm()
    return render(
        request, "analysis_take_form.html", {"form": form, "analysis": analysis_id}
    )


class AnalysisTakeDetailView(LoginRequiredMixin, DetailView):
    model = AnalysisTake
    template_name = "analysis_take_detail.html"


def csv_preview(request, id):
    # Assuming you have a CSV file path or you can modify this to handle uploaded files
    signal = get_object_or_404(SignalAnalysis, pk=id)
    with open(signal.input_signal.name, "r", encoding="utf-8") as file:
        if signal.input_signal.name.split(".")[-1] == ".csv":
            csv_data = csv.reader(file)
        else:
            csv_data = file.read()
        # Convert csv data to list for easy processing in the template
        context = {
            "csv_data": list(csv_data),
        }
    return render(request, "preview_table.html", context)


class SignalAnalysisListView(LoginRequiredMixin, TemplateView):
    template_name = "signal_analysis_list.html"


class TakeTempView(LoginRequiredMixin, TemplateView):
    template_name = "take_new_section.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["take_id"] = kwargs.get("pk")
        context["analysis_id"] = kwargs.get("analysis_id")
        return context


class TakeExisingView(TakeTempView):
    template_name = "take_existing_section.html"


class SignalAnalysisTableView(LoginRequiredMixin, TemplateView):
    template_name = "signal_analysis_table.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["signal_analysis_list"] = SignalAnalysis.objects.filter(
            created_by=self.request.user
        ).order_by("-created")

        return context


class SignalTakesHistoryView(LoginRequiredMixin, TemplateView):
    template_name = "signal_takes_hisory.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        analysis_take_id = kwargs.get("pk")
        analysis_take = get_object_or_404(AnalysisTake, pk=analysis_take_id)
        context["takes"] = AnalysisTake.objects.filter(
            analysis__id=analysis_take.analysis.id
        ).order_by("-created")

        return context


class SignalAnalysisProcessingView(LoginRequiredMixin, TemplateView):
    template_name = "signal_analysis_processing.html"

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super().get_context_data(**kwargs)
        context["analysis"] = self.kwargs.get("analysis_id")
        return context


class TakeProcessingView(LoginRequiredMixin, TemplateView):
    template_name = "take_processing.html"

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super().get_context_data(**kwargs)
        analysis = self.kwargs.get("analysis_id")
        signal = get_object_or_404(SignalAnalysis, pk=analysis)
        take = signal.analysistake_set.last()
        if take.analysis_result is None:
            process_take(take.pk)
            context["processed"] = False
        context["processed"] = True
        context["take"] = take.id
        context["analysis_id"] = signal.id
        return context


class TakeResultView(LoginRequiredMixin, TemplateView):
    template_name = "analysis_take_result.html"

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        take = get_object_or_404(AnalysisTake, pk=pk)
        context["take"] = take
        results_color = "gray"
        if take.analysis_result is None:
            status = "in_progress"
            context["processed"] = False
            return context
        context["processed"] = True
        status = take.analysis_result.get("status")
        if status == "good":
            results_color = "green"
        elif status == "bad":
            results_color = "red"

        context["results_color"] = results_color
        context["results_status"] = status
        return context
