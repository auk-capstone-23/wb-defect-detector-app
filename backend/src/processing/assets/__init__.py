import os
import pickle

import pandas as pd

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


def read_training_data():
    """
    Read the training data from the csv file
    """
    # Read training data
    data = pd.read_csv(os.path.join(__location__, "som_best_100_acc_training_data.csv"))

    training_data = data[["peak_freq", "mean_freq"]]
    labels = data[["label"]]
    return training_data, labels


def load_som_model():
    """
    Load the trained SOM model from the pickle file
    """
    with open(os.path.join(__location__, "som_best_100_acc.p"), "rb") as infile:
        som = pickle.load(infile)
    return som


def load_som_winmap():
    """
    Load the trained SOM model from the pickle file
    """
    with open(os.path.join(__location__, "som_best_100_acc_winmap.p"), "rb") as infile:
        winmap = pickle.load(infile)
    return winmap
