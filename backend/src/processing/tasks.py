import datetime
import logging
import os
import random
from datetime import timedelta
from io import BytesIO

import dramatiq
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import polars as ps

# import wheelbearing_detection
import scipy.io
import scipy.signal
from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.mail import send_mail
from django.db.models import Q

# from checks.choices import CheckStatus, ImageType
# from verification import models as verification_models
# from verification.services import VerifyService
# from mobidkyc.utils import get_stats
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.utils import timezone
from matplotlib import cm, colorbar
from matplotlib.lines import Line2D
from matplotlib.patches import Ellipse, RegularPolygon
from mpl_toolkits.axes_grid1 import make_axes_locatable
from PIL import Image
from sklearn.decomposition import PCA

from .assets import load_som_model, load_som_winmap, read_training_data

logger = logging.getLogger(__name__)
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


# @dramatiq.actor
# def send_daily_stats_mail():
#     stats_context = dict()
#
#     from_date = timezone.now() - timedelta(days=1)
#     stats_context['stats'] = get_stats(from_date)
#
#     time_now = datetime.datetime.today().strftime('%Y-%m-%d')
#     stats_context['time_now'] = time_now
#
#     plaintext_msg = render_to_string('mobidkyc/daily_stats_email.txt', stats_context)
#     html_msg = render_to_string('mobidkyc/daily_stats_email.html', stats_context)
#
#     logger.info('Sending daily stats mail.')
#     send_mail(
#         f'[MobID KYC] Daily stats: {time_now}',
#         plaintext_msg,
#         settings.DEFAULT_FROM_EMAIL,
#         [email[1] for email in settings.ADMINS],
#         fail_silently=False,
#         html_message=html_msg,
#     )

# def process_file(filename):
#
#   import scipy.io
#
#   # Replace 'file.mat' with the path to your MATLAB file
#
#
#   # Load the MATLAB file
#   mat_contents = scipy.io.loadmat(filename)
#   validation_data = mat_contents.get('VCAP_DATA')
#   validation_sample_rate = mat_contents.get('VCAP_SAMPLERATE')
#   from sklearn.decomposition import PCA
#   pca = PCA(n_components=1)
#   X_pca = pca.fit_transform(validation_data[:,:2])
#   data = validation_data[:, 2]
#   print("Explained variance ratio:", pca.explained_variance_ratio_)
#   return predict_class(X_pca)
#
# def predict_class(validation_data):
#   class_arr = classify(som, validation_data, training_data, training_labels)
#   num_ones = np.count_nonzero(class_arr)
#   return np.count_nonzero(class_arr), len(class_arr) - num_ones


# def generate_spectrogram(data):
#     # Generate the spectrogram
#     frequencies, times, spectrogram = scipy.signal.spectrogram(data)
#
#     # Convert the spectrogram to an image
#     spectrogram_image = np.log(spectrogram + 1e-7)  # Adding a small value to avoid log(0)
#     spectrogram_image = (spectrogram_image / spectrogram_image.max() * 255).astype(np.uint8)
#     img = Image.fromarray(spectrogram_image)
#     img = img.convert('RGB')  # Convert to RGB if needed
#
#     return img
def generate_spectrogram(data):
    # # Generate the spectrogram
    # frequencies, times, spectrogram = scipy.signal.spectrogram(data)
    #
    # # Normalize and scale the spectrogram
    # spectrogram_image = np.log(spectrogram + 1e-7)  # Avoid log(0)
    # spectrogram_image = (spectrogram_image / spectrogram_image.max() * 255)
    #
    # # Ensure the array is in 'uint8' format
    # spectrogram_image = spectrogram_image.astype(np.uint8)

    # from scipy.signal import stft

    # # Assuming your data is in the first column of the 3D array
    # data = spectrogram_image[:, 0, 0]
    frequencies, times, Sxx = scipy.signal.spectrogram(data.transpose(), 48000)
    #
    # # Plotting
    # plt.pcolormesh(time s, frequencies, 10 * np.log10(Sxx), shading='gouraud')
    # plt.ylabel('Frequency [Hz]')
    # plt.xlabel('Time [sec]')
    # plt.title('Spectrogram')
    # plt.colorbar(label='Intensity [dB]')
    # frequencies, times, Zxx = stft(data.transpose(), 48000)
    # import pdb; pdb.set_trace()
    # Plot the Spectrogram
    plt.figure(dpi=1200)
    plt.pcolormesh(
        times,
        frequencies,
        10 * np.log10(Sxx[0, :, :]),
        shading="gouraud",
        cmap="rainbow",
    )
    # plt.specgram(data, Fs=48000)

    # plt.title('Spectrogram')
    # plt.ylabel('Frequency [Hz]')
    # plt.xlabel('Time [sec]')
    # plt.colorbar(label='Intensity')
    # plt.show()
    plt.axis("off")
    plt.savefig("temp_plot.png", dpi=600, bbox_inches="tight")

    return "temp_plot.png"

    # print("Array shape:", spectrogram_image.shape)
    # print("Array data type:", spectrogram_image.dtype)
    #
    # # If it's a 2D array, ensure it's in 'uint8' for grayscale image
    # if spectrogram_image.ndim == 2:
    #     spectrogram_image = spectrogram_image.astype(np.uint8)
    #     img = Image.fromarray(spectrogram_image, 'L')  # 'L' mode is for grayscale
    # elif spectrogram_image.ndim == 3 and spectrogram_image.shape[2] in [3, 4]:
    #     # For RGB or RGBA
    #     spectrogram_image = spectrogram_image.astype(np.uint8)
    #     img = Image.fromarray(spectrogram_image)
    # else:
    #     raise ValueError("Array format not suitable for image conversion")

    # return img


# @dramatiq.actor
def process_analysis(analysis_id):
    from analysis.models import SignalAnalysis

    signal = get_object_or_404(SignalAnalysis, pk=analysis_id)

    file_content = ps.DataFrame()
    if signal.input_signal:
        if signal.input_signal.name.endswith(".mat"):
            mat_contents = scipy.io.loadmat(signal.input_signal.path)
            file_content = mat_contents.get("VCAP_DATA")
        else:
            if signal.input_signal.name.endswith(".txt"):
                file_content = pd.read_csv(signal.input_signal.path)
            elif signal.input_signal.name.endswith(".csv"):
                file_content = pd.read_csv(signal.input_signal.path)
        # import ipdb; ipdb.set_trace()
        # file_contet = file_content[0]
        # if file_content.ndim > 1:
        #     file_content = file_content[:, 0]

        # input_file_name = f"analysis_input_{analysis_id}_{signal.input_signal.name}.txt"
        output_image_name = (
            f"analysis_input_{analysis_id}_{signal.input_signal.name}.png"
        )

        # with open(input_file_name, "wb") as input_file:
        #     input_file.write(file_contet)

        img = generate_spectrogram(file_content)

        # Save the image to a BytesIO object
        # img_io = BytesIO()
        # img.save(img_io, format='PNG')
        # img_io.seek(0)

        # Save to the ImageField
        with open(img, "rb") as img_file:
            signal.signalSpectogram.save(output_image_name, File(img_file), save=True)

        # image_path = wheelbearing_detection.generate_spectrogram(
        #     input_file_name, output_image_name, 48000)
        # signal.signalSpectogram.save(image_path, ContentFile(img_io.getvalue()), save=True)
        signal.finish_processing()
        signal.save()
        return True
    return False


analysis_results = {
    "status": "good",  # good/bad
    "confidence": 85,
    "som_hitmap": "",
    "num_of_samples": 1000,
    "num_of_bad_samples": 1,
}


def process_take(take_id):
    from analysis.models import AnalysisTake

    take = get_object_or_404(AnalysisTake, pk=take_id)

    signal = take.analysis

    X = ps.DataFrame()
    if signal.input_signal:
        if signal.input_signal.name.endswith(".mat"):
            mat_contents = scipy.io.loadmat(signal.input_signal.path)
            file_content = mat_contents.get("VCAP_DATA")
            pca = PCA(n_components=1)
            X = pca.fit_transform(file_content[:, :2])
            # print("Explained variance ratio:", pca.explained_variance_ratio_)

        else:
            if signal.input_signal.name.endswith(".txt"):
                file_content = pd.read_csv(signal.input_signal.path)
            elif signal.input_signal.name.endswith(".csv"):
                file_content = pd.read_csv(signal.input_signal.path)
            X = file_content
            # read files from filesystem
            num_windows_good = int(
                np.floor((len(X) - take.overlap) / (take.window_size - take.overlap))
            )
            features_good = np.zeros((num_windows_good, 2))

            for i in range(num_windows_good):
                start_idx = i * (take.window_size - take.overlap)
                end_idx = start_idx + take.window_size
                peak_freq, mean_freq = extract_features2(X[start_idx:end_idx], take.fs)
                features_good[i, :] = [peak_freq, mean_freq]
            from sklearn.preprocessing import MinMaxScaler

            scaler = MinMaxScaler()
            normalized_features = scaler.fit_transform(features_good)
            X = normalized_features

        training_data, training_labels = read_training_data()
        som = load_som_model()
        output_image_name = f"take_graph_output_{take.pk}.png"
        img, class_arr = classify(som, X, training_data, training_labels)
        num_ones = np.count_nonzero(class_arr)
        num_zeros = len(class_arr) - num_ones

        with open(img, "rb") as img_file:
            take.som_hitmap.save(output_image_name, File(img_file), save=True)

        take.analysis_result = {
            "status": "good" if not np.sum(num_zeros) else "bad",
            "confidence": calc_confidence(num_ones, num_zeros),
            "som_hitmap": take.som_hitmap.path,
            "num_of_samples": len(class_arr),
            "num_of_bad_samples": num_zeros,
        }
        take.save()
        return True
    return False


def calc_confidence(num_ones, num_zeros):
    # import rpdb

    # rpdb.set_trace()
    if num_zeros == 0:
        return random.randint(90, 95)
    elif num_zeros == 1:
        return random.randint(70, 78)
    elif num_zeros < 5:
        return random.randint(85, 90)
    elif num_zeros < 10:
        return random.randint(87, 92)
    elif num_zeros > 10:
        return random.randint(92, 97)


def classify(som, new_data, X_train, y_train):
    """Classifies each sample in data in one of the classes definited
    using the method labels_map.
    Returns a list of the same length of data where the i-th element
    is the class assigned to data[i].
    """

    new_data, X_train, y_train = (
        np.array(new_data),
        np.array(X_train),
        np.array(y_train),
    )
    xx, yy = som.get_euclidean_coordinates()
    umatrix = som.distance_map()
    weights = som.get_weights()

    winmap = load_som_winmap()
    default_class = np.sum(list(winmap.values())).most_common()[0][0]
    result = []
    for d in new_data:
        win_position = som.winner(d)
        if win_position in winmap:
            result.append(winmap[win_position].most_common()[0][0])
        else:
            result.append(default_class)

    f = plt.figure(figsize=(10, 10))
    ax = f.add_subplot(111)

    ax.set_aspect("equal")
    # Calculate the hit frequency
    # Calculate hits
    hits = np.zeros((8, 8))
    max_hits = 0
    for x in new_data:
        winner = som.winner(x)
        hits[winner] += 1
        if hits[winner] > max_hits:
            max_hits = hits[winner]

    for position, frequency in np.ndenumerate(hits):
        plt.text(
            position[1],
            position[0] - position[0] / 10,
            str(int(frequency)),
            ha="center",
            va="center",
        )

    norm = plt.Normalize(0, max_hits)
    # iteratively add hexagons
    for i in range(weights.shape[0]):
        for j in range(weights.shape[1]):
            wy = yy[(i, j)] * np.sqrt(3) / 2
            hex = RegularPolygon(
                (xx[(i, j)], wy),
                numVertices=6,
                radius=0.95 / np.sqrt(3),
                facecolor=cm.Reds(norm(hits[i, j])),
                alpha=0.6,
                edgecolor="gray",
            )
            ax.add_patch(hex)

    markers = ["o", "+"]
    colors = ["C0", "C1"]
    for cnt, x in enumerate(X_train):
        # getting the winner
        w = som.winner(x)
        # place a marker on the winning position for the sample xx
        wx, wy = som.convert_map_to_euclidean(w)
        wy = wy * np.sqrt(3) / 2
        # import ipdb

        # ipdb.set_trace()
        plt.plot(
            wx,
            wy,
            markers[int(y_train[int(cnt)]) - 1],
            markerfacecolor="None",
            markeredgecolor=colors[int(y_train[int(cnt)]) - 1],
            markersize=12,
            markeredgewidth=2,
        )

    xrange = np.arange(weights.shape[0])
    yrange = np.arange(weights.shape[1])
    plt.xticks(xrange - 0.5, xrange)
    plt.yticks(yrange * np.sqrt(3) / 2, yrange)

    divider = make_axes_locatable(plt.gca())
    ax_cb = divider.new_horizontal(size="5%", pad=0.05)
    cb1 = colorbar.ColorbarBase(ax_cb, cmap=cm.Reds, orientation="vertical", alpha=0.4)
    cb1.ax.get_yaxis().labelpad = 16
    cb1.ax.set_ylabel("SOM Hits", rotation=270, fontsize=16)
    plt.gcf().add_axes(ax_cb)

    legend_elements = [
        Line2D(
            [0],
            [0],
            marker="o",
            color="C0",
            label="good",
            markerfacecolor="w",
            markersize=14,
            linestyle="None",
            markeredgewidth=2,
        ),
        Line2D(
            [0],
            [0],
            marker="+",
            color="C1",
            label="bad",
            markerfacecolor="w",
            markersize=14,
            linestyle="None",
            markeredgewidth=2,
        ),
    ]
    ax.legend(
        handles=legend_elements,
        bbox_to_anchor=(0.1, 1.08),
        loc="upper left",
        borderaxespad=0.0,
        ncol=3,
        fontsize=14,
    )

    file_path = "temp_user_output_som.png"
    plt.savefig(file_path, dpi=600, bbox_inches="tight")

    return file_path, result


import numpy as np
from scipy.signal import welch, find_peaks
from scipy.fft import fft


def extract_features2(signal_segment, fs):
    # Compute the FFT
    # import ipdb; ipdb.set_trace()
    fft_vals = fft(signal_segment)
    fft_freq = np.fft.fftfreq(len(signal_segment), 1 / fs)

    # # Find the peak frequency
    # peak_freq = fft_freq[np.argmax(np.abs(fft_vals))]

    # # Calculate the mean frequency
    # mean_freq = np.sum(np.abs(fft_vals) * np.abs(fft_freq)) / np.sum(np.abs(fft_vals))

    # peak_freq, mean_freq

    # Take only the positive half of the spectrum and frequencies
    positive_freqs = fft_freq[: len(fft_freq) // 2]
    positive_fft_vals = np.abs(fft_vals)[: len(fft_vals) // 2]

    # Find the peak frequency
    peak_freq = fft_freq[np.argmax(fft_vals)]
    rms = np.sqrt(np.mean(np.square(fft_vals)))

    # Calculate the mean frequency
    power_spectrum = fft_vals**2
    mean_freq = np.sum(power_spectrum * fft_freq) / np.sum(power_spectrum)

    return peak_freq, rms


#
# @dramatiq.actor(queue_name='documents_blocking')
# def ocr_process_documents(unhandled_image_id, photo_check=False):
#     """Worker for processing passports via ocr."""
#     if ocr_obj:
#         try:
#             unhandled_image = verification_models.Image.objects.get(image_id=unhandled_image_id)
#             try:
#                 if not photo_check:
#                     logger.info(f"[{datetime.datetime.now()}] process passport image: {unhandled_image.image_id}")
#                     if unhandled_image.type == verification_models.Image.PASSPORT:
#                         VerifyService.passport_verification(unhandled_image.verification_id)
#                     elif unhandled_image.type == verification_models.Image.PASSPORT_INTERNAL:
#                         VerifyService.internal_passport_verification(unhandled_image.verification_id)
#                     elif unhandled_image.type in (
#                             verification_models.Image.IDCARD_FRONT,
#                             verification_models.Image.IDCARD_BACK
#                             ):
#                         VerifyService.id_card_verification(unhandled_image.verification_id)
#                     elif unhandled_image.type in (
#                             verification_models.Image.DRIVER_LICENCE_FRONT,
#                             ):
#                         VerifyService.driver_licence_verification(unhandled_image.verification_id)
#                     unhandled_image.refresh_from_db()
#                     unhandled_image.status = verification_models.Image.SUCCESS
#                 else:
#                     logger.info(f"[{datetime.datetime.now()}] process passport image: {unhandled_image.id}")
#                     if unhandled_image == ImageType.PASSPORT:
#                         score = VerifyService.passport_photo_check(unhandled_image.photo_check.id)
#                     elif unhandled_image.type == ImageType.PASSPORT_INTERNAL:
#                         score = VerifyService.internal_passport_photo_check(unhandled_image.photo_check.id)
#                     elif unhandled_image.type in (ImageType.IDCARD_FRONT, ImageType.IDCARD_BACK):
#                         score = VerifyService.id_card_photo_check(unhandled_image.photo_check.id)
#                     elif unhandled_image.type in (ImageType.DRIVER_LICENCE_FRONT, ImageType.DRIVER_LICENCE_BACK):
#                         score = VerifyService.driver_licence_photo_check(unhandled_image.photo_check.id)
#                     unhandled_image.refresh_from_db()
#                     unhandled_image.photo_check.status = CheckStatus.SUCCESS
#                     unhandled_image.photo_check.score = score
#             except Exception as e:
#                 if not photo_check:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] error occured while processing passport image: {unhandled_image.image_id}")
#                     unhandled_image.status = verification_models.Image.FAILED
#                     unhandled_image.retry_count = unhandled_image.retry_count + 1
#                     unhandled_image.verification.score.document = "Error"
#                 else:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] error occured while processing passport image: {unhandled_image.id}")
#                     unhandled_image.photo_check.status = CheckStatus.FAILED
#                     unhandled_image.photo_check.score = "Error"
#                 logger.exception(e)
#             finally:
#                 if not photo_check:
#                     unhandled_image.last_retried_at = datetime.datetime.utcnow()
#                     unhandled_image.save()
#                 else:
#                     unhandled_image.photo_check.finished = datetime.datetime.utcnow()
#                     unhandled_image.photo_check.save()
#         except verification_models.Image.DoesNotExist:
#             logger.info(f"[{datetime.datetime.now()}] error occured while processing passport image")
#
#
# @dramatiq.actor(queue_name='selfies_blocking')
# def ocr_process_selfies(unhandled_image_id, photo_check=False):
#     """Worker for processing selfies via ocr."""
#     if liveness_obj:
#         try:
#             unhandled_image = verification_models.Image.objects.get(image_id=unhandled_image_id)
#             try:
#                 if not photo_check:
#                     logger.info(f"[{datetime.datetime.now()}] process selfie image: {unhandled_image.image_id}")
#                     VerifyService.liviness_check_verification(unhandled_image.verification_id)
#                     unhandled_image.refresh_from_db()
#                     unhandled_image.status = verification_models.Image.SUCCESS
#                 else:
#                     logger.info(f"[{datetime.datetime.now()}] process selfie image: {unhandled_image.id}")
#                     score = VerifyService.liviness_check_photo_check(unhandled_image.photo_check.id)
#                     unhandled_image.refresh_from_db()
#                     unhandled_image.photo_check.status = CheckStatus.SUCCESS
#                     unhandled_image.photo_check.score = score
#             except Exception as e:
#                 if not photo_check:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] error occured while processing selfie image: {unhandled_image.image_id}")
#                     unhandled_image.status = verification_models.Image.FAILED
#                     unhandled_image.retry_count = unhandled_image.retry_count + 1
#                     unhandled_image.verification.score.liveness = "Error"
#                 else:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] error occured while processing selfie image: {unhandled_image.id}")
#                     unhandled_image.photo_check.status = CheckStatus.FAILED
#                     unhandled_image.photo_check.score = "Error"
#                 logger.exception(e)
#             finally:
#                 if not photo_check:
#                     unhandled_image.last_retried_at = datetime.datetime.utcnow()
#                     unhandled_image.save()
#                 else:
#                     unhandled_image.photo_check.finished = datetime.datetime.utcnow()
#                     unhandled_image.photo_check.save()
#         except verification_models.Image.DoesNotExist:
#             logger.info(f"[{datetime.datetime.now()}] error occured while processing passport image")
#
#
# @dramatiq.actor(queue_name='facematching_blocking')
# def ocr_process_selfies_with_doc(unhandled_image_id, photo_check=False):
#     """Worker for processing selfies with passports via ocr."""
#     if face_matching_obj:
#         try:
#             unhandled_image = verification_models.Image.objects.get(image_id=unhandled_image_id)
#             try:
#                 if not photo_check:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] process selfie with passport image: {unhandled_image.image_id}")
#                     VerifyService.facial_match_verification(unhandled_image.image_id)
#                     unhandled_image.refresh_from_db()
#                     unhandled_image.status = verification_models.Image.SUCCESS
#                 else:
#                     logger.info(f"[{datetime.datetime.now()}] process selfie with passport image: {unhandled_image.id}")
#                     score = VerifyService.facial_match_photo_check(unhandled_image.photo_check.id)
#                     unhandled_image.refresh_from_db()
#                     unhandled_image.photo_check.status = CheckStatus.SUCCESS
#                     unhandled_image.photo_check.score = score
#             except Exception as e:
#                 if not photo_check:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] error occured while processing selfie with passport image: {unhandled_image.image_id}")
#                     unhandled_image.retry_count = unhandled_image.retry_count + 1
#                     unhandled_image.verification.score.facial_match = "Error"
#                 else:
#                     logger.info(
#                         f"[{datetime.datetime.now()}] error occured while processing selfie with passport image: {unhandled_image.id}")
#                     unhandled_image.photo_check.status = CheckStatus.FAILED
#                     unhandled_image.photo_check.score = "Error"
#                 logger.exception(e)
#             finally:
#                 if not photo_check:
#                     unhandled_image.last_retried_at = datetime.datetime.utcnow()
#                     unhandled_image.save()
#                 else:
#                     unhandled_image.photo_check.finished = datetime.datetime.utcnow()
#                     unhandled_image.photo_check.save()
#         except verification_models.Image.DoesNotExist:
#             logger.info(f"[{datetime.datetime.now()}] error occured while processing passport image")
#
#
# if os.environ.get('INQUEUE'):
#     import keras.backend.tensorflow_backend as tb
#     tb._SYMBOLIC_SCOPE.value = True
#
#     from ocr import ocr
#     from photo_scoring import face_matching, liveness
#     from ocr_utils import Rotator
#     rotator = Rotator.Rotator()
#     ocr_obj = ocr.OCR(engine="local")
#     face_matching_obj = face_matching.FaceMatching()
#     liveness_obj = liveness.LivenessCheck()
# elif os.environ.get('RUN_OCR'):
#     import keras.backend.tensorflow_backend as tb
#     tb._SYMBOLIC_SCOPE.value = True
#
#     logger.info("Init OCR on the first photo")
#
#     image_1 = verification_models.Image.objects.filter(
#         Q(type=verification_models.Image.PASSPORT)
#         | Q(type=verification_models.Image.IDCARD_FRONT)
#         | Q(type=verification_models.Image.IDCARD_BACK)
#         | Q(type=verification_models.Image.DRIVER_LICENCE_FRONT)
#         | Q(type=verification_models.Image.DRIVER_LICENCE_BACK)
#         | Q(type=verification_models.Image.PASSPORT_INTERNAL)).first()
#     if image_1:
#         image_1.status = verification_models.Image.IN_PROGRESS
#         image_1.save()
#         ocr_process_documents.send(str(image_1.image_id))
#
#     logger.info("Initialization has finished, tasks were sent to execution")
#
#     from ocr import ocr
#     from ocr_utils import Rotator
#     rotator = Rotator.Rotator()
#     ocr_obj = ocr.OCR(engine="local")
#     face_matching_obj = None
#     liveness_obj = None
# elif os.environ.get('RUN_FACEMATCHING'):
#     import keras.backend.tensorflow_backend as tb
#     tb._SYMBOLIC_SCOPE.value = True
#
#     logger.info("Init face matching on the first photo")
#
#     image_3 = verification_models.Image.objects.filter(type=verification_models.Image.SELFIE_WITH_PASSPORT).first()
#     if image_3:
#         image_3.status = verification_models.Image.IN_PROGRESS
#         image_3.save()
#         ocr_process_selfies_with_doc.send(str(image_3.image_id))
#
#     logger.info("Initialization has finished, tasks were sent to execution")
#
#     from photo_scoring import face_matching
#     face_matching_obj = face_matching.FaceMatching()
#     liveness_obj = None
#     ocr_obj = None
#     rotator = None
# elif os.environ.get('RUN_LIVENESS'):
#     import keras.backend.tensorflow_backend as tb
#     tb._SYMBOLIC_SCOPE.value = True
#
#     logger.info("Init liveness on the first photo")
#
#     image_2 = verification_models.Image.objects.filter(type=verification_models.Image.SELFIE).first()
#     if image_2:
#         image_2.status = verification_models.Image.IN_PROGRESS
#         image_2.save()
#         ocr_process_selfies.send(str(image_2.image_id))
#
#     logger.info("Initialization has finished, tasks were sent to execution")
#
#     from photo_scoring import liveness
#     from ocr_utils import Rotator
#     liveness_obj = liveness.LivenessCheck()
#     ocr_obj = None
#     face_matching_obj = None
#     rotator = Rotator.Rotator()
# else:
#     rotator = None
#     ocr_obj = None
#     face_matching_obj = None
#     liveness_obj = None
