'use server'

import { z } from 'zod'
import { signalAnalysisSchema } from '@/lib/validation'
import { ApiError, AnalysisCreateError } from '@frontend/types/api'
import { getApiClient } from '@/lib/api'

export type SignalAnalysisSchema = z.infer<typeof signalAnalysisSchema>

export type CreateAnalysisAction = (
  data: SignalAnalysisSchema
) => Promise<AnalysisCreateError | boolean>

const signalAnalysisAction: CreateAnalysisAction = async (data) => {
  try {
    const apiClient = await getApiClient()

    apiClient.analysis.analysisApiV1AnalysisCreate({
      input_signal: data.input_signal,
      title: data.title,
      description: data.description,
      signal_spectogram: data.signal_spectogram
    })

    return true
  } catch (error) {
    if (error instanceof ApiError) {
      return error.body as AnalysisCreateError
    }
  }

  return false
}

export { signalAnalysisAction }

