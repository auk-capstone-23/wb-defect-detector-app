// import { registerAction } from '@/actions/registerAction'
// import RegisterForm from '@/components/forms/RegisterForm'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Register - Turbo'
}

export default function Page({ params }: { params: { id: number } }) {
  return <div>My Analysis: {params.id}</div>
}

