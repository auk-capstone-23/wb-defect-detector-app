"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Home = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, (React.createElement(React.Fragment, null,
                React.createElement("h1", { className: "text-xl font-semibold tracking-tight text-gray-900" }, "Turbo - Django & Next.js starter kit"),
                React.createElement("div", { className: "w-full max-w-6xl mx-auto bg-white rounded-xl shadow-xl overflow-hidden" },
                    React.createElement("nav", { className: "bg-gradient-to-r from-indigo-600 to-purple-600 text-white" },
                        React.createElement("div", { className: "container mx-auto px-6 py-3 flex justify-between items-center" },
                            React.createElement("a", { className: "font-bold text-2xl lg:text-4xl", href: "#" }, "Funky Interface"),
                            React.createElement("div", { className: "font-semibold" },
                                React.createElement("a", { href: "#", className: "hover:bg-purple-700 rounded px-3 py-2" }, "Dashboard"),
                                React.createElement("a", { href: "#", className: "hover:bg-purple-700 rounded px-3 py-2" }, "Past Records")))),
                    React.createElement("div", { className: "container mx-auto p-8" },
                        React.createElement("div", { className: "bg-white rounded shadow-md p-5 mb-10" },
                            React.createElement("h2", { className: "text-3xl font-bold text-indigo-600 mb-2" }, "How it works?"),
                            React.createElement("p", { className: "text-gray-600 mb-4" }, "Description of the process..."),
                            React.createElement("div", { className: "border-2 border-dashed border-indigo-400 rounded p-4 text-center" },
                                React.createElement("a", { href: "#", className: "text-indigo-500 hover:underline" }, "Start New Analysis"))),
                        React.createElement("div", { className: "bg-white rounded shadow-md p-5" },
                            React.createElement("h2", { className: "text-3xl font-bold text-indigo-600 mb-2" }, "Analysis Results"),
                            React.createElement("div", { className: "grid grid-cols-1 md:grid-cols-3 gap-4" },
                                React.createElement("div", { className: "p-4 bg-gradient-to-r from-yellow-200 via-red-200 to-pink-200 rounded shadow-inner" }, "Graph Placeholder"),
                                React.createElement("div", { className: "p-4 bg-gradient-to-r from-green-200 via-blue-200 to-indigo-200 rounded shadow-inner" }, "Graph Placeholder"),
                                React.createElement("div", { className: "p-4 bg-gradient-to-r from-purple-200 via-pink-200 to-red-200 rounded shadow-inner" }, "Graph Placeholder"))),
                        React.createElement("div", { className: "bg-white rounded shadow-md p-5 mt-10" },
                            React.createElement("div", { className: "overflow-x-auto" },
                                React.createElement("table", { className: "min-w-full leading-normal" },
                                    React.createElement("thead", null,
                                        React.createElement("tr", null,
                                            React.createElement("th", { className: "px-5 py-3 bg-indigo-600 text-left text-xs font-semibold text-white uppercase tracking-wider rounded-tl-lg" }, "Date"),
                                            React.createElement("th", { className: "px-5 py-3 bg-indigo-600 text-left text-xs font-semibold text-white uppercase tracking-wider rounded-tr-lg" }, "Record"))),
                                    React.createElement("tbody", null,
                                        React.createElement("tr", null,
                                            React.createElement("td", { className: "px-5 py-5 border-b border-gray-200 text-sm" },
                                                React.createElement("div", { className: "flex items-center" },
                                                    React.createElement("div", { className: "ml-3" },
                                                        React.createElement("p", { className: "text-gray-900 whitespace-no-wrap" }, "Date Placeholder")))),
                                            React.createElement("td", { className: "px-5 py-5 border-b border-gray-200 text-sm" },
                                                React.createElement("p", { className: "text-gray-900 whitespace-no-wrap" }, "Record Placeholder")))))))))))];
    });
}); };
exports["default"] = Home;
