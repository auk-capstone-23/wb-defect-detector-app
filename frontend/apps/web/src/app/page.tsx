const Home = async () => {
  return (
    <>
      <h1 className="text-xl font-semibold tracking-tight text-gray-900">
        Turbo - Django & Next.js starter kit
      </h1>
      

      <div className="w-full max-w-6xl mx-auto bg-white rounded-xl shadow-xl overflow-hidden">




      <nav className="bg-gradient-to-r from-indigo-600 to-purple-600 text-white">

      <div className="container mx-auto px-6 py-3 flex justify-between items-center">

      <a className="font-bold text-2xl lg:text-4xl" href="#">

      Funky Interface

      </a>

      <div className="font-semibold">

      <a href="#" className="hover:bg-purple-700 rounded px-3 py-2">Dashboard</a>

      <a href="#" className="hover:bg-purple-700 rounded px-3 py-2">Past Records</a>

      </div>

      </div>

      </nav>




      <div className="container mx-auto p-8">

      <div className="bg-white rounded shadow-md p-5 mb-10">


      <h2 className="text-3xl font-bold text-indigo-600 mb-2">How it works?</h2>

      <p className="text-gray-600 mb-4">Description of the process...</p>

      <div className="border-2 border-dashed border-indigo-400 rounded p-4 text-center">

      <a href="#" className="text-indigo-500 hover:underline">Start New Analysis</a>

      </div>

      </div>




      <div className="bg-white rounded shadow-md p-5">

      <h2 className="text-3xl font-bold text-indigo-600 mb-2">Analysis Results</h2>

      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">


      <div className="p-4 bg-gradient-to-r from-yellow-200 via-red-200 to-pink-200 rounded shadow-inner">

      Graph Placeholder

      </div>

      <div className="p-4 bg-gradient-to-r from-green-200 via-blue-200 to-indigo-200 rounded shadow-inner">

      Graph Placeholder

      </div>

      <div className="p-4 bg-gradient-to-r from-purple-200 via-pink-200 to-red-200 rounded shadow-inner">

      Graph Placeholder

      </div>

      </div>

      </div>




      <div className="bg-white rounded shadow-md p-5 mt-10">

      <div className="overflow-x-auto">

      <table className="min-w-full leading-normal">

      <thead>

      <tr>

      <th className="px-5 py-3 bg-indigo-600 text-left text-xs font-semibold text-white uppercase tracking-wider rounded-tl-lg">

      Date

      </th>

      <th className="px-5 py-3 bg-indigo-600 text-left text-xs font-semibold text-white uppercase tracking-wider rounded-tr-lg">

      Record

      </th>

      </tr>

      </thead>

      <tbody>


      <tr>

      <td className="px-5 py-5 border-b border-gray-200 text-sm">

      <div className="flex items-center">

      <div className="ml-3">

      <p className="text-gray-900 whitespace-no-wrap">

      Date Placeholder

      </p>

      </div>

      </div>

      </td>

      <td className="px-5 py-5 border-b border-gray-200 text-sm">

      <p className="text-gray-900 whitespace-no-wrap">Record Placeholder</p>

      </td>

      </tr>


      </tbody>

      </table>

      </div>

      </div>

      </div>



      </div>

      </>
      )}

      export default Home;
