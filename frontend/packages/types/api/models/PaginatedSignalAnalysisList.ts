/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SignalAnalysis } from './SignalAnalysis';

export type PaginatedSignalAnalysisList = {
    count?: number;
    next?: string | null;
    previous?: string | null;
    results?: Array<SignalAnalysis>;
};

