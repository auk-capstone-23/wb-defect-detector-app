/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SignalAnalysis = {
    readonly created_by: string;
    input_signal?: string | null;
    title: string;
    description: string;
    signalSpectogram?: string | null;
};

