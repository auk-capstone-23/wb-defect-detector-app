/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PaginatedSignalAnalysisList } from '../models/PaginatedSignalAnalysisList';
import type { PatchedSignalAnalysis } from '../models/PatchedSignalAnalysis';
import type { SignalAnalysis } from '../models/SignalAnalysis';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class AnalysisService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @param page A page number within the paginated result set.
     * @returns PaginatedSignalAnalysisList
     * @throws ApiError
     */
    public analysisApiV1AnalysisList(
        page?: number,
    ): CancelablePromise<PaginatedSignalAnalysisList> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/analysis/api/v1/analysis/',
            query: {
                'page': page,
            },
        });
    }

    /**
     * @param requestBody
     * @returns SignalAnalysis
     * @throws ApiError
     */
    public analysisApiV1AnalysisCreate(
        requestBody: SignalAnalysis,
    ): CancelablePromise<SignalAnalysis> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/analysis/api/v1/analysis/',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id A unique integer value identifying this Signal Analysis.
     * @returns SignalAnalysis
     * @throws ApiError
     */
    public analysisApiV1AnalysisRetrieve(
        id: number,
    ): CancelablePromise<SignalAnalysis> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/analysis/api/v1/analysis/{id}/',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param id A unique integer value identifying this Signal Analysis.
     * @param requestBody
     * @returns SignalAnalysis
     * @throws ApiError
     */
    public analysisApiV1AnalysisUpdate(
        id: number,
        requestBody: SignalAnalysis,
    ): CancelablePromise<SignalAnalysis> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/analysis/api/v1/analysis/{id}/',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id A unique integer value identifying this Signal Analysis.
     * @param requestBody
     * @returns SignalAnalysis
     * @throws ApiError
     */
    public analysisApiV1AnalysisPartialUpdate(
        id: number,
        requestBody?: PatchedSignalAnalysis,
    ): CancelablePromise<SignalAnalysis> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/analysis/api/v1/analysis/{id}/',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id A unique integer value identifying this Signal Analysis.
     * @returns void
     * @throws ApiError
     */
    public analysisApiV1AnalysisDestroy(
        id: number,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/analysis/api/v1/analysis/{id}/',
            path: {
                'id': id,
            },
        });
    }

}
